import pytest
import requests
import time
import json

@pytest.fixture
def get_json():
    return requests.get('http://{}:{}{}'.format("127.0.0.1", 8091, '/bear/2'), timeout=2.0).json()


@pytest.yield_fixture(scope='module')
def get_js(request):
    return requests.get(request, timeout=2.0).json()


@pytest.yield_fixture(scope='module')
def client_rest(request):
    """ Simple rest api client. 

    Methods
    -------
    get_json(request):  req to endpoint and return json
    """
    client = ClientTRATask()
    yield client



class ClientTRATask():
    
    def __init__(self, host="localhost", port=8091):
        super(ClientTRATask, self).__init__()
        self.host = host
        self.port = port
        
    def create_bear(self, bear_type="POLAR", bear_name="ivan", bear_age=17.5):
        res = requests.post('http://{}:{}{}'.format(self.host, self.port, "/bear"), json={"bear_type": bear_type, "bear_name": bear_name, "bear_age": bear_age})
        assert res.status_code in [200, 201]
        return res
    
    def update_bear(self, bear_id, bear_type="POLAR", bear_name="ivan", bear_age=16.7):
        if bear_age is not None:
            obj = {"bear_type": bear_type, "bear_name": bear_name, "bear_age": bear_age}
        else:
            obj = {"bear_type": bear_type, "bear_name": bear_name}
        res = requests.put('http://{}:{}{}{}'.format(self.host, self.port, "/bear/", bear_id), json=obj)
        assert res.status_code in [200, 201]
        return res
    
    def delete_all_bears(self):
        res = requests.delete('http://{}:{}{}'.format(self.host, self.port, "/bear"))
        assert res.status_code in [200, 204]
        return res.text
    
    def delete_by_id(self, bear_id):
        res = requests.delete('http://{}:{}{}{}'.format(self.host, self.port, "/bear/", bear_id))
        assert res.status_code in [200, 204]
        return res.text

    def get_bear_id_list(self): 
        return list(map(lambda dict: dict["bear_id"], self.get_all_bears()))

    def get_all_bears(self):
        res = requests.get('http://{}:{}{}'.format(self.host, self.port, "/bear"))
        assert res.status_code == 200
        return res.json()
    
    def get_bear_by_id(self, bear_id):
        res = requests.get('http://{}:{}{}{}'.format(self.host, self.port, "/bear/", bear_id))
        assert res.status_code == 200
        return res.json()
    
    def get_bearresponse_by_id(self, bear_id):
        res = requests.get('http://{}:{}{}{}'.format(self.host, self.port, "/bear/", bear_id))
        
        return res.status_code
    
    def get_json(self, reqq):
        res = requests.get('http://{}:{}{}'.format(self.host, self.port, reqq), timeout=2.0)
        assert res.status_code == 200
        return res.json()
    
    def post_json(self, reqq):
        res = requests.post('http://{}:{}{}'.format(self.host, self.port, reqq), json={"bear_type": "BLACK", "bear_name": "ivan3", "bear_age": 11.1})
        assert res.status_code in [200, 201]
