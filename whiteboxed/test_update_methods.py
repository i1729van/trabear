def test_create_and_update_bear(client_rest):
    """
        В данном тесте добавим медведя и изменим параметры
    """
    # Добавим одного
    client_rest.create_bear("POLAR", "ivan1", 22.0)
    # получаем id последнего (макс из списка)
    newby_id = max(client_rest.get_bear_id_list())
    
    # достаем по id и проверяем
    newby_bear = client_rest.get_bear_by_id(newby_id)
    assert newby_bear["bear_name"].lower() == "ivan1"
    assert newby_bear["bear_type"].lower() == "polar"
    assert newby_bear["bear_age"] == 22.0
    
    # UPDATE'им его по id и проверяем
    client_rest.update_bear(newby_id, "POLAR", "ivan7778", 24.0)
    updated_bear = client_rest.get_bear_by_id(newby_id)
    assert updated_bear["bear_name"].lower() == "ivan7778"
    assert updated_bear["bear_type"].lower() == "polar"
    assert updated_bear["bear_age"] == 22.0
    