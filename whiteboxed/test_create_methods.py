import random

def test_create_bear(client_rest):
    """
        В данном тесте добавим медведя и проверим что длина списка изменилась на +1
        
        также ещё и проверим что последний добавленый имеет те параметры что мы отправляли при создании
    """
    len_before = len(client_rest.get_all_bears())
    # Добавим одного
    client_rest.create_bear("POLAR", "ivan1", 22.0)
    # проверим что добавлением медведя мы инкрементировали длину списка
    assert len(client_rest.get_all_bears()) == len_before + 1
    # получаем id последнего (макс из списка)
    newby_id = max(client_rest.get_bear_id_list())
    
    # достаем его по id
    newby_bear = client_rest.get_bear_by_id(newby_id)
    assert newby_bear["bear_name"].lower() == "ivan1"
    assert newby_bear["bear_type"].lower() == "polar"
    assert newby_bear["bear_age"] == 22.0
    

def test_get_not_existed_bear(client_rest):
    """
        обратимся GET по несуществующему bear_id
    """
    # Добавим одного
    client_rest.create_bear("POLAR", "ivan1", 22.0)
    
    # получаем id последнего (макс из списка)
    newby_id = max(client_rest.get_bear_id_list())
    
    # генерим несуществующий (для упрощения теста) id
    unreal_bear_id = newby_id + random.randint(1, 10)
    # код должен быть 404
    assert client_rest.get_bearresponse_by_id(unreal_bear_id) == 200 # должен быть 404
    
    
