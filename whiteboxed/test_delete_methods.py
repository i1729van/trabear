def test_delete_all_bears(client_rest):
    """
        В данном тесте произведём рандомное добавление трех медведей,
        а потом удалим их все так, чтобы длина списка была равна 0 после операции
    """
    client_rest.create_bear("POLAR", "ivan1", 22.0)
    client_rest.create_bear("POLAR", "ivan2", 23.0)
    client_rest.create_bear("POLAR", "ivan3", 24.0)
    
    # проверяем что точно не пустой список медведей
    assert len(client_rest.get_all_bears()) != 0
    
    # удаляем все записи, ещё в ответ ждем "OK"
    assert client_rest.delete_all_bears().lower() == "ok"
    
    # проверяем что ТОЧНО пуст список медведей
    assert len(client_rest.get_all_bears()) == 0
    


def test_delete_bears_by_id(client_rest):
    """
        В данном тесте создадим рандомные записи из медведей,
        потом будем удалять их по id который получим обратившись ко всему списку медведей
    """
    client_rest.create_bear("POLAR", "ivan4", 25.0)
    client_rest.create_bear("BROWN", "ivan5", 26.0)
    client_rest.create_bear("GUMMY", "ivan6", 27.0)
    
    # проверяем что точно не пустой список медведей
    assert len(client_rest.get_all_bears()) != 0
    id_list = client_rest.get_bear_id_list()
    for bear_id in id_list:
        status = client_rest.delete_by_id(bear_id)
        assert status.lower() == "ok"
        
    # проверяем что ТОЧНО удалили всех медведей
    assert len(client_rest.get_all_bears()) == 0
    
    
def test_delete_not_existed_bear(client_rest):
    """
        заполним рандомными записями и
        попытаемся удалить несуществующего медведя
    """
    client_rest.create_bear("POLAR", "ivan4", 25.0)
    client_rest.create_bear("BROWN", "ivan5", 26.0)
    client_rest.create_bear("GUMMY", "ivan6", 27.0)
    
    # проверяем что точно не пустой список медведей
    assert len(client_rest.get_all_bears()) != 0
    id_list = client_rest.get_bear_id_list()
    
    # получаем id которое точно не в списке
    out_of_range_id = max(id_list) + 5 # получаем последний id и накидываем 5
    status = client_rest.delete_by_id(out_of_range_id)
    assert status.lower() == "ok"
    
    # удаляем все записи, ещё в ответ ждем "OK"
    assert client_rest.delete_all_bears().lower() == "ok"