# Bear tester

Для запуска самих тестов понадобится:
* python3 (only)
* pytest
* requests
* docker (только для запуска сервиса медведей)
    
## Preinstall steps:

### Изменяем параметры (адрес сервиса)
Для смены ip адреса и порта укажите значениe в conftest.py файле:
    
    conftest.py
    
### Запуск сервиса в контейнере 
Для запуска конейнера и получения доступа к порту сервиса (как раз по дефолту для тестов):

```sh 
$ docker run -p 8091:8091 azshoo/alaska:1.0
```
    
## Installation and Run:

  В директории проекта сборка и запуск выполняется командой:
  
```sh 
$ git clone https://gitlab.com/i1729van/trabear.git
$ pytest -vv trabear/
```

